package com.userservice.user.application.share;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JwtTokenServiceTest {

    String signature = "WkJOnjAKoNsLjLguVkzoBU6n9uq2Klfl5sKlRnhFiLD30XjN5rNmQJ7lx7VpsKh71tlZl1YYteNVX4PoQgjPkHZMm9pfV35E8wn" +
            "dPUTOHpAxVgfX9zsUrRGv5zGUkFZgi6quyVGf348xTTBaxLX337haTpWGQUp4s4cGXNT7HYPkmypPhlosRdShp2HiH8YIMOa9R7zn42ysUNVperTvNs1kz1qyXxr5B4gQ1wGVvDqEKLByPqdzU1ANLIWHBcf1";
    String jwtTrue = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJTYW50aWFnbyBHb256YWxleiIsImlhdCI6MTYyODQ1NDkwMX0" +
            ".9LRYIyh0Bi0oMMEImekW-4rP6OSQ4c-oWRolIn-A3Lk";
    String jwtFalse = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ" +
            ".kCXvojg5Gnop5s9Pf8qxmEgPN3qZmrZxPLwjeOcv41U";

    @Test
    void createToken() {
        JwtTokenService tokenService = new JwtTokenService();
        tokenService.setSignature(signature);
        tokenService.setTyp("jwt");
        tokenService.setAlg("HS256");

        String jwt = tokenService.createToken("Santiago Gonzalez");

        assertNotNull(jwt);
    }

    @Test
    void validateTrue() {
        JwtTokenService tokenService = new JwtTokenService();
        tokenService.setSignature(signature);

        boolean validate = tokenService.validate(jwtTrue);

        assertTrue(validate);
    }

    @Test
    void validateFalse() {
        JwtTokenService tokenService = new JwtTokenService();
        tokenService.setSignature(signature);

        boolean validate = tokenService.validate(jwtFalse);

        assertFalse(validate);
    }
}