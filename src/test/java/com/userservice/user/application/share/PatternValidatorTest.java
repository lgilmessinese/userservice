package com.userservice.user.application.share;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatternValidatorTest {

    @Test
    void validateEmail() {
        boolean validate = PatternValidator.validateEmail("santiago@gonzalez.org");
        assertTrue(validate);
    }

    @Test
    void validatePassword() {
        boolean validate = PatternValidator.validatePassword("Hunter23");
        assertTrue(validate);
    }
}