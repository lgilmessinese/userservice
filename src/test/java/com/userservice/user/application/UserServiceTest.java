package com.userservice.user.application;

import com.userservice.user.application.share.JwtTokenService;
import com.userservice.user.domain.model.Phone;
import com.userservice.user.domain.model.User;
import com.userservice.user.infraestructure.UserRepository;
import com.userservice.user.interfaces.rest.exception.NotFoundException;
import com.userservice.user.interfaces.rest.exception.ValidateException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    private static final String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9";
    private User user;

    @Mock
    private UserRepository userRepository;
    @Mock
    private JwtTokenService tokenBuilder;

    @InjectMocks
    UserService service;

    @BeforeEach
    void setUp() {
        user = User.builder().email("santiago@gonzalez.org").password("Hunter23")
                .token(TOKEN).phones(Collections.singletonList(Phone.builder().build())).build();
    }

    @Test
    @DisplayName("Sign up user")
    void signUp() {
        when(userRepository.findByEmail(any())).thenReturn(null);
        when(userRepository.save(any())).thenReturn(user);
        when(tokenBuilder.createToken(any())).thenReturn(TOKEN);

        User userSaved = service.signUp(this.user);

        assertNotNull(userSaved);
        assertEquals(TOKEN, userSaved.getToken());
    }

    @Test
    @DisplayName("Sign up user when email already exists")
    void signUp_alreadyEmail() {
        when(userRepository.findByEmail(any())).thenReturn(user);

        assertThrows(ValidateException.class, () -> service.signUp(this.user));
    }

    @Test
    @DisplayName("Sign up user when email is invalid")
    void signUp_validateEmail() {
        when(userRepository.findByEmail(any())).thenReturn(null);
        user.setEmail("email");

        assertThrows(ValidateException.class, () -> service.signUp(this.user));
    }

    @Test
    @DisplayName("Sign up user when password is invalid")
    void signUp_validatePassword() {
        when(userRepository.findByEmail(any())).thenReturn(null);
        user.setPassword("password");

        assertThrows(ValidateException.class, () -> service.signUp(this.user));
    }

    @Test
    @DisplayName("Login user")
    void login() {
        when(userRepository.findByKey(any())).thenReturn(user);

        User userResponse = service.login("123");

        assertNotNull(userResponse);
    }

    @Test
    @DisplayName("Login user when not found")
    void login_notFound() {
        assertThrows(NotFoundException.class, () -> service.login("123"));
    }


}