package com.userservice.user.interfaces.rest.exception;

import com.userservice.user.interfaces.rest.dto.ErrorResponseDto;
import com.userservice.user.interfaces.rest.dto.NotificationResponseDto;
import com.userservice.user.interfaces.rest.share.NotificationBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserControllerAdviceTest {

    @Mock
    private NotificationBuilder<Exception> notification;

    @InjectMocks
    private UserControllerAdvice advice;

    @Test
    @DisplayName("Not found exception")
    void noFoundException() {
        String errorCode = "404";
        when(notification.getResponse(any(), any())).thenReturn(
                new ResponseEntity<>(NotificationResponseDto.builder().error(Collections.singletonList(ErrorResponseDto.builder()
                        .code(errorCode).build())).build(), HttpStatus.NOT_FOUND));

        ResponseEntity<NotificationResponseDto> responseEntity =
                advice.noFoundException(new NotFoundException(errorCode), mock(WebRequest.class));

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(errorCode, responseEntity.getBody().getError().get(0).getCode());
    }

    @Test
    @DisplayName("Authorization exception")
    void authorizationException() {
        String errorCode = "401";
        when(notification.getResponse(any(), any())).thenReturn(
                new ResponseEntity<>(NotificationResponseDto.builder().error(Collections.singletonList(ErrorResponseDto.builder()
                        .code(errorCode).build())).build(), HttpStatus.UNAUTHORIZED));

        ResponseEntity<NotificationResponseDto> responseEntity =
                advice.authorizationException(new AuthenticationException(errorCode), mock(WebRequest.class));

        assertEquals(HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(errorCode, responseEntity.getBody().getError().get(0).getCode());
    }

    @Test
    @DisplayName("Validate exception")
    void validateException() {
        String errorCode = "500";
        when(notification.getResponse(any(), any())).thenReturn(
                new ResponseEntity<>(NotificationResponseDto.builder().error(Collections.singletonList(ErrorResponseDto.builder()
                        .code(errorCode).build())).build(), HttpStatus.INTERNAL_SERVER_ERROR));

        ResponseEntity<NotificationResponseDto> responseEntity =
                advice.validateException(new ValidateException(errorCode), mock(WebRequest.class));

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(errorCode, responseEntity.getBody().getError().get(0).getCode());
    }
}