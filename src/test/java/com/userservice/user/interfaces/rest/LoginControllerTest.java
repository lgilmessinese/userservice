package com.userservice.user.interfaces.rest;

import com.userservice.user.application.UserPort;
import com.userservice.user.domain.model.User;
import com.userservice.user.interfaces.rest.dto.SuccessResponseDto;
import com.userservice.user.interfaces.rest.dto.UserDto;
import com.userservice.user.interfaces.rest.mapper.UserMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class LoginControllerTest {

    private User user;
    private UserDto userDto;
    @Mock
    private UserPort userPort;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private LoginController controller;

    @BeforeEach
    void setUp() {
        user = User.builder().created(LocalDateTime.now()).lastLogin(LocalDateTime.now()).modified(LocalDateTime.now())
                .phones(Collections.EMPTY_LIST).build();
        userDto = UserDto.builder().phones(Collections.EMPTY_LIST).build();
    }

    @Test
    @DisplayName("Sign up user")
    void signUp() {
        when(userMapper.mapUserDtoToUser(userDto)).thenReturn(user);
        when(userPort.signUp(user)).thenReturn(user);
        when(userMapper.mapUserToSuccessResponseDto(user)).thenReturn(new SuccessResponseDto());


        ResponseEntity<SuccessResponseDto> responseEntity = controller.signUp(userDto);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
    }

    @Test
    @DisplayName("Login user")
    void login() {
        Principal principal = () -> "123-123";
        when(userPort.login(anyString())).thenReturn(user);
        when(userMapper.mapUserToUserDto(user)).thenReturn(userDto);

        ResponseEntity<UserDto> responseEntity = controller.login(principal);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
    }
}