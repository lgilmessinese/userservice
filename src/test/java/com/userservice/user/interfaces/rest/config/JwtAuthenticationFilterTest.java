package com.userservice.user.interfaces.rest.config;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class JwtAuthenticationFilterTest {

    private JwtAuthenticationFilter jwtAuthenticationFilter;
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private FilterChain filterChain;

    @BeforeEach
    void setUp() throws IllegalAccessException, NoSuchFieldException {
        jwtAuthenticationFilter = new JwtAuthenticationFilter();
        Field field = JwtAuthenticationFilter.class.getDeclaredField("secretKey");
        field.setAccessible(true);
        field.set(jwtAuthenticationFilter, "WkJOnjAKoNsLjLguVkzoBU6n9uq2Klfl5sKlRnhFiLD30XjN5rNmQJ7lx7VpsKh71tlZl1YYteNVX4PoQgjPkHZMm9pfV35E8wn" +
                "dPUTOHpAxVgfX9zsUrRGv5zGUkFZgi6quyVGf348xTTBaxLX337haTpWGQUp4s4cGXNT7HYPkmypPhlosRdShp2HiH8YIMOa9R7zn42ysUNVperTvNs1kz1qyXxr5B4gQ1wGVvDqEKLByPqdzU1ANLIWHBcf1");

        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        filterChain = mock(FilterChain.class);

    }


    @Test
    @DisplayName("Should not filter")
    void shouldNotFilter() {
        request.setRequestURI("/sign-up");

        assertTrue(jwtAuthenticationFilter.shouldNotFilter(request));
    }

    @Test
    @DisplayName("Do filter internal")
    void doFilterInternal() throws ServletException, IOException {
        String token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJjZGVjZmQyNi1mN2QwLTRjZWYtYjQzYi1jZjllMm" +
                "Y5MTBhM2IiLCJpYXQiOjE3MTIzMzUzNzl9.TVyyw0ocZ-wegEw9TnhTorYE1UfbJVVQn4aYTicdap4"; // replace with a valid token
        request.addHeader("Authorization", token);

        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        assertNotNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    @DisplayName("Do filter internal - no token")
    void doFilterInternal_noToken() throws ServletException, IOException {
        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    @DisplayName("Do filter internal - token does not start with Bearer")
    void doFilterInternal_tokenDoesNotStartWithBearer() throws ServletException, IOException {
        String token = "NotBearerToken";
        request.addHeader("Authorization", token);
        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    @DisplayName("Do filter internal - exception while parsing token")
    void doFilterInternal_exceptionWhileParsingToken() throws ServletException, IOException {
        String token = "Bearer invalidToken";
        request.addHeader("Authorization", token);
        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        assertNull(SecurityContextHolder.getContext().getAuthentication());
        assertEquals(HttpServletResponse.SC_FORBIDDEN, response.getStatus());
    }
}