package com.userservice.user.interfaces.rest.share;

import com.userservice.user.interfaces.rest.dto.NotificationResponseDto;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

class NotificationBuilderTest {

    @Test
    void getResponse() {
        NotificationBuilder<Exception> notification = new NotificationBuilder<>();
        ResponseEntity<NotificationResponseDto> response = notification.getResponse(new Exception(), HttpStatus.INTERNAL_SERVER_ERROR);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }
}