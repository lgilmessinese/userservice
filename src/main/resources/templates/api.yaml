openapi: 3.0.3
info:
  title: User Register
  description: user manager service
  version: 1.0.0
servers:
  - url: 'http://localhost:8090/user-service/api/v1'
    description: Local url
paths:
  '/users':
    get:
      tags:
        - User
      summary: Allow to get user into system
      description: Get information about users
      operationId: get_users
      security:
        - bearerAuth: [ ]
      responses:
        '200':
          description: Retrieve all Users
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Users'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
    post:
      tags:
        - User
      summary: Allow to create a user
      description: Create a user into system
      operationId: save_user
      security:
        - bearerAuth: [ ]
      requestBody:
        required: true
        description: events object to save into database
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
      responses:
        '200':
          description: Retrieve notification created user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NotificationSuccess'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
  '/users/{userId}':
    get:
      tags:
        - User
      summary: Allow to get a user
      description: get a user
      operationId: get_user
      security:
        - bearerAuth: [ ]
      parameters:
        - name: userId
          in: path
          description: user's Id
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Retrieve a user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
    delete:
      tags:
        - User
      summary: Allow to delete a user
      description: delete a user
      operationId: delete_user
      security:
        - bearerAuth: [ ]
      parameters:
        - name: userId
          in: path
          description: user's Id
          required: true
          schema:
            type: string
      responses:
        '200':
          description: delete a user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Notification'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
    put:
      tags:
        - User
      summary: Allow to update a user
      description: update a user
      operationId: update_user
      security:
        - bearerAuth: [ ]
      parameters:
        - name: userId
          in: path
          description: user's Id
          required: true
          schema:
            type: string
      requestBody:
        required: true
        description: events object to save into database
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
      responses:
        '200':
          description: update a user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
components:
  securitySchemes:
    bearerAuth: # arbitrary name for the security scheme
      type: http
      scheme: bearer
      in: header
  schemas:
    Users:
      type: object
      properties:
        data:
          type: array
          items:
            $ref: '#/components/schemas/User'
    User:
      type: object
      description: Information user object
      properties:
        name:
          type: string
          description: user name
          example: Juan Rodriguez
        email:
          type: string
          description: user email
          example: juan@rodriguez.org
        password:
          type: string
          description: user password
          example: hunter2
        phones:
          type: array
          items:
            $ref: '#/components/schemas/Phone'
    Phone:
      type: object
      description: Information phone object
      properties:
        number:
          type: string
          description: number phone
          example: 1234567
        citycode:
          type: string
          description: city code phone
          example: 1
        countrycode:
          type: string
          description: country code phone
          example: 57
    Notification:
      type: object
      description: Information object
      properties:
        uuid:
          type: string
          description: Universally unique identifier by notification
          example: 8d134566-86c5-4523-9dcc-2a33e5fd57b5
        timestamp:
          type: string
          format: date-time
          description: Timestamp by notification
          example: 2021-07-01T14:59:55.711Z
        code:
          type: string
          description: Error code
          example: E3456SF3KNSDF021
        message:
          type: string
          description: Message error
          example: message error
    NotificationSuccess:
      type: object
      description: Information object
      properties:
        id:
          type: string
          description: Universally unique identifier by notification
          example: 8d134566-86c5-4523-9dcc-2a33e5fd57b5
        created:
          type: string
          format: date-time
          description: Timestamp by notification
          example: 2021-07-01T14:59:55.711Z
        modified:
          type: string
          format: date-time
          description: Timestamp by notification
          example: 2021-07-01T14:59:55.711Z
        last_login:
          type: string
          description: Error code
          example: 2021-07-01T14:59:55.711Z
        token:
          type: string
          description: token
          example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c
        isactive:
          type: string
          description: active user
          example: true
  responses:
    Success:
      description: User successful register
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Notification'
    Unauthorized:
      description: User can't be access to resource
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Notification'
    NotFound:
      description: Resource can´t be found
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Notification'
    InternalServerError:
      description: Problem appeared on the server
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Notification'