CREATE TABLE users (
    usr_id INT AUTO_INCREMENT PRIMARY KEY,
    usr_key VARCHAR(250) NOT NULL,
    usr_name VARCHAR(250) NOT NULL,
    usr_email VARCHAR(250) NOT NULL,
    usr_password VARCHAR(250) NOT NULL,
    usr_created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    usr_modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    usr_last_login TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    usr_token VARCHAR(250) NOT NULL,
    usr_active BIT NOT NULL DEFAULT 1
);

CREATE TABLE phones (
    phn_id INT AUTO_INCREMENT PRIMARY KEY,
    phn_number VARCHAR(250) NOT NULL,
    phn_city_code VARCHAR(250) NOT NULL,
    phn_country_code VARCHAR(250) NOT NULL,
    usr_id INT NOT NULL,
    CONSTRAINT FK_PhoneUsers FOREIGN KEY (usr_id) REFERENCES users(usr_id)
);

INSERT INTO users (usr_id, usr_key, usr_name, usr_email, usr_password, usr_token) VALUES
(1, '5387a45f-98f3-4e74-be36-913da43ed047', 'Juan Rodriguez', 'juan@rodriguez.org', 'hunter2',
 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lI\iwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'),
(2, '3bd60be2-e2f2-4e03-8c21-f3593d0a9b1f', 'Pedro Perez', 'pedro@perez.org', 'hunter3',
 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lI\iwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'),
(3, 'ae0ad8cb-0ce1-4062-b769-91aec0972f5d', 'Lucas Hernandez', 'lucas@hernandez.org', 'hunter4',
 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lI\iwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c');

INSERT INTO phones (phn_id, phn_number, phn_city_code, phn_country_code, usr_id) VALUES
(1, '1234567', '1', '57', 1),
(2, '7890345', '3', '58', 1),
(3, '4567812', '5', '59', 2),
(4, '9876543', '5', '57', 3);