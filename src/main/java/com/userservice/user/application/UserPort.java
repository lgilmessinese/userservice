package com.userservice.user.application;

import com.userservice.user.domain.model.User;

import java.util.List;

public interface UserPort {

    User signUp(User user);

    User login(String key);

}
