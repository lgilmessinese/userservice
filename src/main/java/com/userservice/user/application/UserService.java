package com.userservice.user.application;

import com.userservice.user.application.share.JwtTokenService;
import com.userservice.user.application.share.PatternValidator;
import com.userservice.user.domain.enums.ValidationKey;
import com.userservice.user.domain.model.User;
import com.userservice.user.infraestructure.UserRepository;
import com.userservice.user.interfaces.rest.exception.NotFoundException;
import com.userservice.user.interfaces.rest.exception.ValidateException;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Log4j2
@Service
@AllArgsConstructor
public class UserService implements UserPort {

    private final UserRepository userRepository;
    private final JwtTokenService tokenBuilder;


    @Override
    public User signUp(User user) {
        log.info("signUp user: {}", user.getName());

        if (!Objects.isNull(userRepository.findByEmail(user.getEmail()))) {
            throw new ValidateException(ValidationKey.ALREADY_EMAIL.getDescription());
        }

        if (!PatternValidator.validateEmail(user.getEmail())) {
            throw new ValidateException(ValidationKey.INVALID_EMAIL.getDescription());
        }

        if (!PatternValidator.validatePassword(user.getPassword())) {
            throw new ValidateException(ValidationKey.INVALID_PASSWORD.getDescription());
        }

        String key = UUID.randomUUID().toString();
        user.setKey(key);
        user.setCreated(LocalDateTime.now());
        user.setModified(LocalDateTime.now());
        user.setLastLogin(LocalDateTime.now());
        user.setToken(tokenBuilder.createToken(key));
        user.setActive(true);
        user.getPhones().forEach(phone -> phone.setUser(user));
        return userRepository.save(user);
    }

    @Override
    public User login(String key) {
        log.info("getUserByKey");
        User userResponse = userRepository.findByKey(key);
        if (Objects.isNull(userResponse)) throw new NotFoundException("User: " + key + " not found");

        return userResponse;
    }


}
