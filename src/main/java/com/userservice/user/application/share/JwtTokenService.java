package com.userservice.user.application.share;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.util.Date;

@Data
@Log4j2
@Component
@NoArgsConstructor
@ConfigurationProperties("api.token")
public class JwtTokenService {

    public static final String NAME = "name";
    private String alg;
    private String typ;
    private String signature;

    public String createToken(String userKey) {
        SecretKey key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(this.getSignature()));

        JwtBuilder builder = Jwts.builder()
                .setHeaderParam(JwsHeader.ALGORITHM, this.getAlg())
                .setHeaderParam(Header.TYPE, this.getTyp())
                .setSubject(userKey)
//                .setExpiration(new Date(System.currentTimeMillis() + 3600000))
                .setIssuedAt(new Date());

        return builder.signWith(key, SignatureAlgorithm.valueOf(this.getAlg())).compact();
    }

    public boolean validate(String jwt) {
        try {
            Jwts.parserBuilder().setSigningKey(this.getSignature()).build().parseClaimsJws(jwt);
        } catch (JwtException e) {
            log.error(e.getMessage(), e);
            return false;
        }

        return true;
    }
}
