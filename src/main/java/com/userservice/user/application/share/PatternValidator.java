package com.userservice.user.application.share;

import java.util.regex.Pattern;

public class PatternValidator {

    public static final Pattern VALID_EMAIL_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    public static final Pattern VALID_PASSWORD_REGEX =
            Pattern.compile("([A-Z ]{1,})([a-z ]{1,})(\\d{2,})", Pattern.CASE_INSENSITIVE);

    public static boolean validateEmail(String email) {
        return VALID_EMAIL_REGEX.asMatchPredicate().test(email);
    }

    public static boolean validatePassword(String password) {
        return VALID_PASSWORD_REGEX.asMatchPredicate().test(password);
    }
}
