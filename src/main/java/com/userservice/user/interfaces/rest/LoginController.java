package com.userservice.user.interfaces.rest;

import com.userservice.user.application.UserPort;
import com.userservice.user.interfaces.rest.dto.SuccessResponseDto;
import com.userservice.user.interfaces.rest.dto.UserDto;
import com.userservice.user.interfaces.rest.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@Log4j2
@RestController
@RequiredArgsConstructor
public class LoginController {

    private final UserPort userPort;
    private final UserMapper userMapper;

    @PostMapping(value = "/sign-up", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SuccessResponseDto> signUp(@RequestBody UserDto userDto) {
        log.info("signUp");

        var user = userPort.signUp(userMapper.mapUserDtoToUser(userDto));

        return ResponseEntity.status(HttpStatus.CREATED).body(userMapper.mapUserToSuccessResponseDto(user));
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> login(Principal principal) {
        log.info("signUp with principal: {}", principal.getName());

        var user = userPort.login(principal.getName());

        return ResponseEntity.status(HttpStatus.OK).body(userMapper.mapUserToUserDto(user));
    }
}
