package com.userservice.user.interfaces.rest.share;

import com.userservice.user.interfaces.rest.dto.ErrorResponseDto;
import com.userservice.user.interfaces.rest.dto.NotificationResponseDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Collections;

@Log4j2
@Component
public class NotificationBuilder <T extends Exception>{

    public ResponseEntity<NotificationResponseDto> getResponse(T ex, HttpStatus httpStatus){
        log.info("NotificationBuilder " + ex.getMessage());
        NotificationResponseDto body = NotificationResponseDto.builder().error(Collections.singletonList(ErrorResponseDto.builder().timestamp(LocalDateTime.now().toString())
                .code(String.valueOf(httpStatus.value()))
                .detail(ex.getMessage())
                .build())).build();

        return new ResponseEntity<>(body, httpStatus);

    }

}