package com.userservice.user.interfaces.rest.exception;

public class NotFoundException  extends RuntimeException{

    public NotFoundException(final String message){
        super(message);
    }

}
