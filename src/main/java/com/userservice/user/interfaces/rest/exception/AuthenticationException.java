package com.userservice.user.interfaces.rest.exception;

public class AuthenticationException extends RuntimeException{

    public AuthenticationException(final String message, final Throwable cause){
        super(message, cause);
    }

    public AuthenticationException(final String message){
        super(message);
    }
}
