package com.userservice.user.interfaces.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserDto {

    private String key;
    private String name;
    private String email;
    private String password;
    private String created;
    private String modified;

    @JsonProperty("lastLogin")
    private String lastLogin;

    @JsonProperty("isActive")
    private boolean active;

    private List<PhoneDto> phones;
}
