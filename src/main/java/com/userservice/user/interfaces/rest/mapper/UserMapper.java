package com.userservice.user.interfaces.rest.mapper;

import com.userservice.user.domain.model.Phone;
import com.userservice.user.domain.model.User;
import com.userservice.user.interfaces.rest.dto.PhoneDto;
import com.userservice.user.interfaces.rest.dto.SuccessResponseDto;
import com.userservice.user.interfaces.rest.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.stream.Collectors;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface UserMapper {

    UserDto mapUserToUserDto(User user);

    @Mapping(target = "name", source = "name")
    @Mapping(target = "email", source = "email")
    @Mapping(target = "password", source = "password")
    @Mapping(target = "active", ignore = true)
    User mapUserDtoToUser(UserDto user);


    Phone mapPhoneDtoToPhone(PhoneDto phoneDto);

    @Mapping(target = "id", source = "key")
    @Mapping(target = "created", source = "created")
    @Mapping(target = "lastLogin", source = "lastLogin")
    @Mapping(target = "token", source = "token")
    @Mapping(target = "active", source = "active")
    SuccessResponseDto mapUserToSuccessResponseDto(User user);
}
