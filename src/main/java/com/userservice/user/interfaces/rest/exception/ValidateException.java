package com.userservice.user.interfaces.rest.exception;

public class ValidateException extends RuntimeException{

    public ValidateException(final String message){
        super(message);
    }

}
