package com.userservice.user.interfaces.rest.exception;

import com.userservice.user.interfaces.rest.dto.NotificationResponseDto;
import com.userservice.user.interfaces.rest.share.NotificationBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Log4j2
@ControllerAdvice
@AllArgsConstructor
public class UserControllerAdvice extends ResponseEntityExceptionHandler {

    private final NotificationBuilder<Exception> notificationBuilder;

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<NotificationResponseDto> noFoundException(NotFoundException e, WebRequest request){
        log.info("noFoundException " + e.getMessage());
        return notificationBuilder.getResponse(e, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = AuthenticationException.class)
    public ResponseEntity<NotificationResponseDto> authorizationException(AuthenticationException e, WebRequest request){
        log.info("authorizationException " + e.getMessage());
        return notificationBuilder.getResponse(e, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = ValidateException.class)
    public ResponseEntity<NotificationResponseDto> validateException(ValidateException e, WebRequest request){
        log.info("validateException " + e.getMessage());
        return notificationBuilder.getResponse(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
