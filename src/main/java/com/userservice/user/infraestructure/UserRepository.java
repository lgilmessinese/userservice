package com.userservice.user.infraestructure;

import com.userservice.user.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByKey(String key);

    User findByEmail(String email);

}
