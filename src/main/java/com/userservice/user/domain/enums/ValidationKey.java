package com.userservice.user.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ValidationKey {
    ALREADY_EMAIL("The email is already"),
    INVALID_EMAIL("The email is invalid format"),
    INVALID_PASSWORD("The pasword is invalid format");

    private final String description;
}
