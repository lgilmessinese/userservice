# Spring Boot User

_This project contain the follow requirement_

* Memory Database H2
* Gradle builder
* Junit test
* Hibernate persistence ORM
* Java 11

## Getting Started 🚀

_This instruction allow you to get a copy into local machine with the aim to develop and testing._

Look at **Deployment** to know how to deploy the project.


### Prerequisites 📋

_You need to have installed the follow toolkit_

* JDK 11+
* Gradle

### Installation 🔧

1. Go the code in the follow link [https://userservice.com](https://gitlab.com/lgilmessinese/userservice)
2. Clone the repo
 ```sh
   git clone https://gitlab.com/lgilmessinese/userservice.git
   ```
## Deploy 📦

_Follow the instruction to run the application. The server run in **8090** port_

1. Get into the folder you clone
 ```sh
   cd userservice
   ```
2. Run gradle projet
 ```sh
   ./gradlew bootRun
   ```
## Usage ⚙️

_Into the folder **collection** has a postman collection with all request present into the service_

### Postman 🔩

_Once loaded the collection into postman, and the app running, the first request to use is **POST** user_
_This gonna response somebody like that_
```
{
    "id": "39d543ae-d778-46f0-a5ba-670437e010f5",
    "created": "2021-08-08T22:14:06.319076",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJTYW50aWFnbyBHb256YWxleiIsImlhdCI6MTYyODQ3NTI0Nn0.PhU_zx0ZagpglNOqtEmLME4W40aK1QJZNWJ0AwQ5Lw4",
    "lastLogin": "2021-08-08T22:14:06.319143",
    "isActive": true
}
```
_If you try to use another request before **POST** user, you're going to get the follow response_
```
{
  "error": [{
    "timestamp": "2021-08-08T22:13:42.412714",
    "code": "401",
    "detail": "Unauthorized error"
  }] 
}
```
_That happen because you need a token to get grants to access the others requests_

### DataBase H2 ⌨️

_The database run in memory, so each time you run the app you gonna lost all changes_
_To access the database copy the follow url into browser_
```
http://localhost:8090/users-service/api/v1/h2-console
```
_Set the next values to get into database console_
```
JDBC URL: jdbc:h2:mem:testdb
User Name: sa
Password: password
```
#### Default data
_When you run the app, this save three test users into database. This data you can be found at:_
```
src/main/resources/static/data.sql
```
## Documentation 📄

_In this section you can get diagram, api swagger and test info_

### Diagram 🖇️
_There are two diagram into **diagram** folder_
```
component_diagram.png
secuence_diagram.png
```

### Swagger API 🛠️
_There is a swagger API_
```
src/main/resources/templates/api.yaml
```

### Test 🛠️
_There are many test that you can see into this package. This test has 88% coverage_
```
src/test/java/
```